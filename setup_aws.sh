#!/bin/bash

function jsonval () 
{
	trimmed_input=$(echo $1 | sed 's/ //g')
    temp=`echo $trimmed_input | sed 's/\\\\\//\//g' | sed 's/[{}]//g' | awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]}' | sed 's/\"\:\"/\|/g' | sed 's/[\,]/ /g' | sed 's/\"//g' | grep -w $2`
    echo ${temp##*|}
}



# One time setup on gateway machine
# ===========================================
# install python (Todo)
curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
sudo python get-pip.py
sudo pip install awscli

# configutre aws client
aws configure



SECURITY_GROUP_NAME=gauss-test-sg
CIDR=0.0.0.0/0
KEY_PAIR=gauss-test-key
KEY_PAIR_DIR=~/aws/keypair
SECURITY_GROUP_DIR=~/aws/security_groups

mkdir -p $KEY_PAIR_DIR
mkdir -p $SECURITY_GROUP_DIR


touch ${SECURITY_GROUP_DIR}/security_group.txt
# create a security group and save the id in the text file below

SECURITY_GROUP=$(aws ec2 create-security-group --group-name $SECURITY_GROUP_NAME --description "security group for development environment in EC2")
GROUP_ID=$(jsonval "$SECURITY_GROUP" "GroupId")
echo $GROUP_ID,$(date) >> ${SECURITY_GROUP_DIR}/security_group.txt

# Replace the CIDR range in the above with the one that you will connect from for more security (Todo)
# Todo: need to configure inbound & outbound traffic
	# All TCP|TCP|0-65535|0.0.0.0/0
	# HTTPS|TCP|443|0.0.0.0/0
	# Custom TCP Rule|TCP|5901|0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name $SECURITY_GROUP_NAME --protocol tcp --port 22 --cidr $CIDR
aws ec2 authorize-security-group-ingress --group-name $SECURITY_GROUP_NAME --protocol tcp --port 0-65535 --cidr $CIDR
aws ec2 authorize-security-group-ingress --group-name $SECURITY_GROUP_NAME --protocol tcp --port 443 --cidr $CIDR
aws ec2 authorize-security-group-ingress --group-name $SECURITY_GROUP_NAME --protocol tcp --port 5901 --cidr $CIDR

# create a key pair, which allows you to connect to the instance
aws ec2 create-key-pair --key-name $KEY_PAIR --query 'KeyMaterial' --output text > ${KEY_PAIR_DIR}/${KEY_PAIR}.pem
chmod 400 ${KEY_PAIR_DIR}/${KEY_PAIR}.pem
# ===========================================