
# Todo: UNTESTED!!!

PEM_FILE=/Users/oliversaleh/work/Gauss/aws_pems/ec2machine_key.pem
EC2MACHINE=ec2-52-10-164-70.us-west-2.compute.amazonaws.com

ssh -tt -i "$PEM_FILE" ec2-user@$EC2MACHINE 'bash -s' << EOF
	# make a backup of docker website
	# switch to root user
	sudo su -;
	echo Starting backup process;
	DATE=`date +%Y-%m-%d`;
	TARGET_DIR=/tmp/gausswebsite_docker_image_snapshots/\$DATE;
	mkdir -p \$TARGET_DIR;
	# create a snapshot image of the current running containers (assuming they havent been stopped - otherwise do a "docker ps" to get their id's)
	GAUSS_WEBSITE_CONTAINER_ID=0c8159da5e1a;
	GAUSS_MYSQL_CONTAINER_ID=b7148a8425a8;

	docker commit \$GAUSS_WEBSITE_CONTAINER_ID gausswebsite-wordpress/website;
	docker commit \$GAUSS_MYSQL_CONTAINER_ID gausswebsite-wordpress/msql;
	# save the snapshot images to file
	docker save -o \$TARGET_DIR/gausswebsite-wordpress-website-\$DATE-SNAPSHOT.tar gausswebsite-wordpress/website;
	docker save -o \$TARGET_DIR/gausswebsite-wordpress-mysql-\$DATE-SNAPSHOT.tar gausswebsite-wordpress/msql;

	# need to change the owner and permissions
	chmod 755 -R /tmp/gausswebsite_docker_image_snapshots
	chmod 755 -R \$TARGET_DIR
	sudo chown -R ec2-user:ec2-user \$TARGET_DIR

	# remove the docker images created
	docker rmi gausswebsite-wordpress/website gausswebsite-wordpress/msql
	exit;
	exit;
EOF

# copy the files over to local machine for backup

# Todo: this may be different than the date on the ec2 machine!! fix
DATE=`date +%Y-%m-%d`
TARGET_DIR="/tmp/gausswebsite_docker_image_snapshots/$DATE"
mkdir -p $GAUSS_REPO_ROOT/website_backups/
scp -i $PEM_FILE ec2-user@$EC2MACHINE:$TARGET_DIR/gausswebsite-wordpress-website-$DATE-SNAPSHOT.tar $GAUSS_REPO_ROOT/website_backups/
scp -i $PEM_FILE ec2-user@$EC2MACHINE:$TARGET_DIR/gausswebsite-wordpress-mysql-$DATE-SNAPSHOT.tar $GAUSS_REPO_ROOT/website_backups/


echo "Cleaning up changes on ec2"
ssh -tt -i "$PEM_FILE" ec2-user@$EC2MACHINE 'bash -s' << EOF

	sudo su -;
	DATE=`date +%Y-%m-%d`;
	TARGET_DIR="/tmp/gausswebsite_docker_image_snapshots/\$DATE";
	# remove the snapshots
	echo yes | rm -rf \$TARGET_DIR;
	exit;
	exit;
EOF


# # note: to install the images:
# GAUSS_MYSQL_PASSWORD=gauss123
# GAUSS_WEBSITE_CONTAINER_NAME=gaussinsights-wordpress-website
# GAUSS_MYSQL_CONTAINER_NAME=gaussinsights-wordpress-mysql
# sudo docker load -i $TARGET_DIR/gausswebsite-wordpress-website-$DATE-SNAPSHOT.tar
# sudo docker load -i $TARGET_DIR/gausswebsite-wordpress-mysql-$DATE-SNAPSHOT.tar
# # create a running container pair
# docker run --name $GAUSS_MYSQL_CONTAINER_NAME -e MYSQL_ROOT_PASSWORD=$GAUSS_MYSQL_PASSWORD -d gausswebsite-wordpress/msql:latest
# docker run --name $GAUSS_WEBSITE_CONTAINER_NAME --link $GAUSS_MYSQL_CONTAINER_NAME:mysql -p 8080:80 -d gausswebsite-wordpress/website:latest
# # note: you can also do this in another ec2 container but first you need to setup docker: 
# 	sudo yum install docker 
# 	sudo su -
# 	service docker start
