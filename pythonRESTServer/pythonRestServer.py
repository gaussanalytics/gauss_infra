from flask import Flask, url_for, json, request, Response, jsonify
from functools import wraps
import subprocess
from subprocess import call

# +++++++++++++++++++++++++++++++++++++++++
# To run this server
# 1. install flask:
# 	pip install flask
# 2. set the FLASK_PATH to the location of this script
# 	export FLASK_APP=/Users/oliversaleh/Desktop/pythonRESTServer/pythonRestServer.py
# 3. start the flask server on port 8089
# 	flask run -p 8089
# 4. To kill it hit Ctrl+C
# +++++++++++++++++++++++++++++++++++++++++

# Todo: test this on python 3

app = Flask(__name__)

# Note: these two file names have to correspond to the ones in the crontab
configFileName="conf.properties"
outputFileName="output.log"
#============================================================================
#=========== Helper Functions ===============================================

# Authorizing access
def check_auth(username, password):
	# Todo: change the password here
    return username == 'admin' and password == 'secret'

def authenticate():
    message = {'message': "Unauthorized"}
    resp = jsonify(message)

    resp.status_code = 401
    resp.headers['WWW-Authenticate'] = 'Basic realm="Example"'

    return resp

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth: 
            return authenticate()
        elif not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated


def create_file():
	try:
		fh = open(configFileName, 'r')
		msg="Config File Already Exists"
		print(msg)
		return msg
	except IOError:
		fh = open(configFileName, 'w')
		msg="Config File Created!"
		print(msg)
		return msg

def apply_new_config(data):
	print("Applying configuration changes...")
	print("-------------------------------")
	create_file()
	# Todo: implement this
	fh = open(configFileName, 'w')
	fh.write(data)
	print("Setting config to: " + data)
	msg="Config File updated successfully"
	print("-------------------------------")
	return (True,msg)
	
def get_current_config():
	print("Getting current configuration...")
	print("-------------------------------")
	try:
		fh = open(configFileName, 'r')
		data = fh.read()
		print("Config File retreived successfully")
		print("-------------------------------")
		return (True,data)
	except IOError:
		msg="Config File does not exist!"
		print(msg)
		print("-------------------------------")
		return (False,msg)


def stop_cron_service():
	print("Stopping CRON Service...")
	print("-------------------------------")
	
	# run in background
	# s = subprocess.Popen("./test.sh > myout.txt &", shell=True)
	#subprocess.call("test.sh", shell=True)
	# to wait for process to finish use:
	s = subprocess.Popen("service crond stop", shell=True)
	s.wait()
	# Todo: get status and make sure it started properly
	# get the status and make sure its stopped

	#print("Process initiated in the background...")
	msg="Cron Service Successfuly stopped!"
	# Todo: get status and make sure it started properly
	print("-------------------------------")
	return (True,msg)

def start_cron_service():
	print("Starting CRON Service...")
	print("-------------------------------")
	s = subprocess.Popen("service crond start", shell=True)
	s.wait()
	msg="Cron Service Successfuly started!"
	# Todo: get status and make sure it started properly
	print("-------------------------------")
	return (True,msg)

def restart_cron_service():
	print("Restarting CRON Service...")
	print("-------------------------------")
	s = subprocess.Popen("service crond restart", shell=True)
	s.wait()
	msg="Cron Service Successfuly restarted!"
	# Todo: get status and make sure it started properly
	print("-------------------------------")
	return (True,msg)

def update_cron_tab():
	print("Updating Cron Tab file...")
	print("-------------------------------")
	
	print("Doing Work...TBD")

	print("-------------------------------")
	return (False,"TBD")

def force_run():
	print("Forcefully running ETL service (not initiated by cron file)!...")
	print("-------------------------------")
	# run in background
	# Todo: modify this script
	cmd="Rscript /usr/local/src/gaussinsights/socialmediaanalytics/scripts/ETLInfluencers.R -w \"/usr/local/src/gaussinsights/socialmediaanalytics/scripts\" -c \"Nestle\" -P \"/usr/lib/python2.7\" -n \"iri-dataset.c2mehnsdsldj.us-west-2.redshift.amazonaws.com\" -p \"5439\" -d \"iridataset\" -u \"oliver\" -s \'qnTT5+9*45eeW!jodP\' -k \"#nestle,#kitkat,#nescafe,nestle chocolate,nestle,nestle water,nestle coffee,nescafe,smarties,skinny cow,tuttles,coffee-mate,coffee mate,maggi,milo,coffee crisp,kit kat,kitkat\""

	s = subprocess.Popen(cmd + " >> " + outputFileName + " &", shell=True)
	#subprocess.call("test.sh", shell=True)
	# to wait for process to finish use:
	
	# Todo: get the status and make sure its started

	msg="ETL script Successfully invoked"
	# Todo: get status and make sure it started properly
	print("-------------------------------")
	return (True,msg)

def force_stop():
	print("Forcefully stopping ETL service (if cron service active it wil be re-run after some time)!...")
	print("-------------------------------")
	
	print("Doing Work...TBD")

	print("-------------------------------")
	return (False,"TBD")

def get_output():
	print("Getting output of ETL process...")
	print("-------------------------------")
	try:
		fh = open(outputFileName, 'r')
		data = fh.read()
		print("Output Log File retreived successfully!")
		print("-------------------------------")
		return (True,data)
	except IOError:
		msg="Output Log File does not exist!"
		print(msg)
		print("-------------------------------")
		return (False,msg)

#============================================================================
#========== REST Interface ==================================================

@app.route('/setconfig', methods = ['POST'])
@requires_auth
def api_set_config_params():
    if request.headers['Content-Type'] == 'text/plain':
    	res = apply_new_config(request.data)
    	return res[1]

    elif request.headers['Content-Type'] == 'application/json':
    	res = apply_new_config(json.dumps(request.json))
    	return res[1]

    elif request.headers['Content-Type'] == 'application/octet-stream':
    	# Todo: fix this
        f = open('./binary', 'wb')
        f.write(request.data)
        f.close()
        return "Binary message written!"

    else:
        return "415 Unsupported Media Type"

@app.route('/getconfig', methods = ['GET'])
@requires_auth
def api_get_config_params():
	result = get_current_config()
	return result[1]
	# if(result(0) == False):
		
	# else:
	# 	return result

# @app.route('/isrunning', methods = ['GET'])
# @requires_auth
@app.route('/stopcronservice', methods = ['GET'])
@requires_auth
def api_stop_cron_service():
	result = stop_cron_service()
	return result[1]
	


@app.route('/startcronservice', methods = ['GET'])
@requires_auth
def start_cron_service():
	result = start_cron_service()
	return result[1]


@app.route('/restartcronservice', methods = ['GET'])
@requires_auth
def restart_cron_service():
	result = restart_cron_service()
	return result[1]


@app.route('/forcerun', methods = ['GET'])
@requires_auth
def api_force_start():
	result = force_run()
	return result[1]


@app.route('/forcestop', methods = ['GET'])
@requires_auth
def api_force_stop():
	result = force_stop()
	return result[1]


# @app.route('/forcerun', methods = ['GET'])
# @requires_auth

# @app.route('/createconfig')
# def api_root():
# 	return create_file()
    
@app.route('/articles/<articleid>')
def api_article(articleid):
    return 'You are reading ' + articleid

#============================================================================


if __name__ == '__main__':
    app.run()
