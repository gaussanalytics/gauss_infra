import SimpleHTTPServer
import SocketServer
class myHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
   def do_GET(self):
       print self.path
       self.send_response(307)
       new_path = '%s%s'%('https://www.youtube.com', self.path)
       self.send_header('Location', new_path)
       self.end_headers()


PORT = 8000
handler = SocketServer.TCPServer(("", PORT), myHandler)
print "serving at port 8000"
handler.serve_forever()



# see here for better implementation: https://wiki.python.org/moin/BaseHttpServer
# use ctrl+c to kill
# source: http://stackoverflow.com/questions/2506932/how-do-i-forward-a-request-to-a-different-url-in-python
# http://stackoverflow.com/questions/9020162/avoiding-301-redirect-caching
# http://stackoverflow.com/questions/14525029/display-a-loading-message-while-a-time-consuming-function-is-executed-in-flask
# http://www.netavatar.co.in/2011/05/31/how-to-show-a-loading-gif-image-while-a-page-loads-using-javascript-and-css/