#!/bin/bash
function jsonval () {
    temp=`echo $1 | sed 's/\\\\\//\//g' | sed 's/[{}]//g' | awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]}' | sed 's/\"\:\"/\|/g' | sed 's/[\,]/ /g' | sed 's/\"//g' | grep -w $2`
    echo ${temp##*|}
}

json="{\"GroupId\":\"sg-b018ced5\"}"
prop='GroupId'
groupid=$(jsonval $json $prop)
echo $groupid