

IMAGE_ID=ami-b04e92d0
EC2_INSTANCE_TYPE=t2.micro
GROUP_ID=sg-8194fdf8
KEY_PAIR=gauss-test-key
KEY_PAIR_DIR=~/aws/keypair
EC2_USER_NAME=ec2-user

ACTIVE_EC2_INSTANCES=~/aws/active_ec2_instances
mkdir -p $ACTIVE_EC2_INSTANCES
# Todo: cretae VPC, IAM role
# Launch ec2 instance & connect to it
# ===========================================
EC2_INSTANCE=$(aws ec2 run-instances --image-id $IMAGE_ID --security-group-ids $GROUP_ID --count 1 --instance-type $EC2_INSTANCE_TYPE --key-name $KEY_PAIR --query 'Instances[0].InstanceId')
EC2_INSTANCE=$(echo $EC2_INSTANCE | sed 's/\"//g')

# retrieve the public IP address that you will use to connect to the instance
PUBLIC_IP=
ATEMPTS=0
 while [  $ATEMPTS -lt 6 ]; do
     echo "Querying IP Address of host..."
     let ATEMPTS=ATEMPTS+1 
     PUBLIC_IP=$(aws ec2 describe-instances --instance-ids $EC2_INSTANCE --query 'Reservations[0].Instances[0].PublicIpAddress')
     if [ -n "$PUBLIC_IP" ]; then
     	PUBLIC_IP=$(echo $PUBLIC_IP | sed 's/\"//g')
     	echo "IP: "$PUBLIC_IP
     	break
     fi
     sleep 10
 done
 echo $EC2_INSTANCE,$PUBLIC_IP,$(date),ACTIVE >> ${ACTIVE_EC2_INSTANCES}/running_instances.txt

# connect to ec2 machine
# Todo prevent from asking yes/no (always yes)
ssh -i ${KEY_PAIR_DIR}/${KEY_PAIR}.pem $EC2_USER_NAME@$PUBLIC_IP << EOF
	# once in console run
	sudo yum update -y
	sudo yum install -y docker
	sudo yum install -y tcpdump
	sudo service docker start
	sudo usermod -a -G docker ec2-user
	exit
EOF
# now log in again
ssh -i ${KEY_PAIR_DIR}/${KEY_PAIR}.pem $EC2_USER_NAME@$PUBLIC_IP << EOF
	hostname
	docker info
EOF


# configure machine and install :
# -network monitoring script
# source: https://unix.stackexchange.com/questions/19485/how-to-monitor-incoming-http-requests
SCRIPTS_DIR=~/aws/scripts
PORT_NUMBER=8080
MONITOR_INTERVAL=2
mkdir -p $SCRIPTS_DIR
NETORK_MONITOR=$SCRIPTS_DIR/network_monitor.sh
rm $NETORK_MONITOR
touch $NETORK_MONITOR
echo "while true" > $NETORK_MONITOR
echo "do" >> $NETORK_MONITOR
echo "echo -----\`date '+%r'\` -----:" >> $NETORK_MONITOR
echo "netstat -ant | grep :$PORT_NUMBER | awk '{print \$6}' | sort | uniq -c | sort -n" >> $NETORK_MONITOR
echo "echo httpd processes: ['ps aux | grep httpd | wc -l']" >> $NETORK_MONITOR
echo "echo ." >> $NETORK_MONITOR
echo "sleep $MONITOR_INTERVAL" >> $NETORK_MONITOR
echo "done" >> $NETORK_MONITOR
chmod +x $NETORK_MONITOR
# -docker containers from S3


# ===========================================